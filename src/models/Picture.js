const mongoose = require("mongoose");
const aws = require("aws-sdk");
const fs = require("fs");
const path = require("path");
const { promisify } = require("util");
const Schema = mongoose.Schema;



const s3 = new aws.S3();

    
const PictureSchema = new mongoose.Schema({

    storehouse:{type:String},
    volume:{type:String},
    archive:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Archive'
    },
    name: String,
    size: Number,
    page:{
        type: Number,
        required:false
    },
    key: String,
    url: String,
    diretorio:String,
    createdAt:{
        type: Date,
        default: Date.now
    }
})

PictureSchema.pre('save', function(){
    if (!this.url){
        this.url = `${process.env.APP_URL}/files/${this.key}`
    }
})

PictureSchema.pre('remove', function(){
    if (process.env.STORAGE_TYPE === 's3'){
        return s3
        .deleteObject({
            Bucket: process.env.BUCKET,
            Key: this.key,
            name:this.name
        }).promise()
    } else {
        return promisify(fs.unlink)(
          path.resolve(__dirname, "..", "..", "tmp", "uploads", this.key)
        );
      }
    });


module.exports = mongoose.model('Picture',PictureSchema)